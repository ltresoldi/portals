$.fn.qtip.styles.cops_portal = { // Last part is the name of the style
		'font-size' : "inherit",
		'width':300,
		'font-family' : 'inherit',
		'font-style' : 'normal',
		'textAlign': 'left',
		tip: {
			corner: 'topMiddle',
			size: {
				x: 16,
				y: 6
			}
		},
		background:'#f7f7f7',
		border: {width: 1, radius:0, color:'#dfdfdf'},
		name: 'light', // Inherit the rest of the attributes from the preset dark style
		classes : {tooltip:'qtip-overflow-normal'}
}


window.reshare_tooltip_style = 'cops_portal';
window.fixed_tooltip_style = 'cops_portal';
window.demand_tooltip_style = 'cops_portal';
window.hint_tooltip_style = 'cops_portal';
